package com.thoughtworks.springbootemployee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Company {
    private Long id;
    private String name;
}
