package com.thoughtworks.springbootemployee.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@AllArgsConstructor
@Getter
public class Employee implements Serializable {
    @Setter
    private Long id;
    private String name;
    @Setter
    private Integer age;
    private String gender;
    @Setter
    private Integer salary;
    private Long companyId;
}
